import 'dart:math';

import 'package:flutter/material.dart';
import 'package:imc_calculator/constants.dart';

class CalulateImc {
  final double height;
  final int weight;

  CalulateImc({
    required this.height,
    required this.weight,
  });

  double get imc => (weight / pow(height / 100, 2));

  var _imcType = imcTypes.noresult;

  get imcType => _imcType;

  calculate() {
    if (imc < 18.5) {
      _imcType = imcTypes.underweight;
    } else if (imc >= 18.5 && imc < 25) {
      _imcType = imcTypes.normal;
    } else if (imc >= 25 && imc < 30) {
      _imcType = imcTypes.overweight;
    } else if (imc >= 30 && imc < 40) {
      _imcType = imcTypes.obese;
    } else if (imc >= 40) {
      _imcType = imcTypes.overobese;
    } else {
      _imcType = imcTypes.noresult;
    }
  }

  Map<Enum, dynamic> results = {
    imcTypes.underweight: {
      'text': 'Magreza',
      'color': Colors.amber.shade200,
      'interpretation': 'Você está abaixo do peso ideal. Cuidado!',
      'range': 'menor que 18,5 kg/m²',
    },
    imcTypes.normal: {
      'text': 'Normal',
      'color': const Color(0xFF24D876),
      'interpretation': 'Você está no peso ideal. Parabéns!',
      'range': 'entre 18,5 - 25 kg/m²',
    },
    imcTypes.overweight: {
      'text': 'Sobrepeso',
      'color': Colors.amber.shade200,
      'interpretation': 'Você está acima do peso ideal. Cuidado!',
      'range': 'entre 25 - 30 kg/m²',
    },
    imcTypes.obese: {
      'text': 'Obesidade',
      'color': Colors.amber.shade700,
      'interpretation': 'Você está obeso. Cuidado!',
      'range': 'entre 30 - 40 kg/m²',
    },
    imcTypes.overobese: {
      'text': 'Obesidade Mórbida',
      'color': Colors.redAccent,
      'interpretation': 'Você está obeso muito. Cuidado!',
      'range': 'maior que 40 kg/m²',
    },
    imcTypes.noresult: {
      'text': '',
      'color': Colors.white,
      'interpretation': '',
    },
  };
}
