import 'package:imc_calculator/home_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(const IMCCalculatorApp());

class IMCCalculatorApp extends StatelessWidget {
  const IMCCalculatorApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        primaryColor: const Color(0xFF0A0E21),
        scaffoldBackgroundColor: const Color(0xFF0A0E21),
        appBarTheme: const AppBarTheme(
          backgroundColor: Color(0xFF0A0E21),
        ),
      ),
      home: const HomePage(),
    );
  }
}
