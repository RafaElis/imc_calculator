import 'package:imc_calculator/constants.dart';
import 'package:imc_calculator/widgets/bottom_button_widget.dart';
import 'package:imc_calculator/widgets/card_widget.dart';
import 'package:flutter/material.dart';

class ResultPage extends StatelessWidget {
  final String imc;
  final String resultTitle;
  final String interpretation;
  final Color resultColor;
  final String resultRange;

  const ResultPage({
    required this.imc,
    required this.resultTitle,
    required this.interpretation,
    required this.resultColor,
    required this.resultRange,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('IMC CALCULATOR'),
        elevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(15),
            child: const Text("Seu Resultado", style: kNumberTextSytle),
          ),
          Expanded(
            child: CardWideget(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  const SizedBox(height: 50),
                  Text(
                    resultTitle.toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 22,
                      color: resultColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    imc,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 100,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '$resultTitle  IMC range:',
                    textAlign: TextAlign.center,
                    style: kLabelTextStyle,
                  ),
                  const SizedBox(height: 4),
                  Text(
                    resultRange,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 18.0),
                    child: Text(
                      interpretation,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                      ),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const Spacer(),
                ],
              ),
            ),
          ),
          BottomButtomWidget(
            text: 'RE-CALCULAR SEU IMC',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
