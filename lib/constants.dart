import 'package:flutter/material.dart';

const Color kCardColor = Color(0xFF1D1E33);
const Color kInactiveCardColour = Color(0xFF111328);
const Color kTextColor = Color(0xFF8D8E98);

const double kBottomContainerHeight = 80.0;

const kLabelTextStyle = TextStyle(
  fontSize: 18,
  color: kTextColor,
);

const kNumberTextSytle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.w900,
);

const kBottomButtonStyle = TextStyle(fontSize: 25, fontWeight: FontWeight.bold);

enum imcTypes {
  underweight,
  normal,
  overweight,
  obese,
  overobese,
  noresult,
}
