import 'package:imc_calculator/constants.dart';
import 'package:flutter/material.dart';

class BottomButtomWidget extends StatelessWidget {
  final String text;
  final Function onPressed;
  const BottomButtomWidget({
    required this.text,
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () => onPressed(),
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: kBottomButtonStyle,
      ),
      style: TextButton.styleFrom(
        minimumSize: const Size(double.infinity, kBottomContainerHeight),
        primary: Colors.white,
        backgroundColor: const Color(0xffeb1555),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
      ),
    );
  }
}
