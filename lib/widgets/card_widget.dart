import 'package:imc_calculator/constants.dart';
import 'package:flutter/cupertino.dart';

class CardWideget extends StatelessWidget {
  final Widget child;
  final Color? color;
  final VoidCallback? onPressed;
  const CardWideget({
    required this.child,
    this.color = kCardColor,
    this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        margin: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: child,
      ),
    );
  }
}
