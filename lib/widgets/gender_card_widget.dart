import 'package:imc_calculator/constants.dart';
import 'package:imc_calculator/widgets/card_widget.dart';
import 'package:flutter/material.dart';

const Map<String, dynamic> genderMap = {
  'male': {
    'text': "MASCULINO",
    'icon': Icons.male,
  },
  'female': {
    'text': "FEMININO",
    'icon': Icons.female,
  },
};

class GenderCardWidget extends StatelessWidget {
  final String gender;
  final String selectedGender;
  final VoidCallback onPressed;
  const GenderCardWidget(
    this.gender, {
    required this.selectedGender,
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: CardWideget(
        color: selectedGender == gender ? kCardColor : kInactiveCardColour,
        onPressed: onPressed,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              genderMap[gender]['icon'],
              size: 80,
              color: gender == selectedGender ? Colors.white : kTextColor,
            ),
            const SizedBox(height: 15),
            Text(
              genderMap[gender]['text'],
              style: kLabelTextStyle,
            ),
          ],
        ),
      ),
    );
  }
}
