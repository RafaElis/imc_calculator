import 'package:flutter/material.dart';

class RoundIconButtonWidget extends StatelessWidget {
  final IconData icon;
  final VoidCallback? onPressed;
  const RoundIconButtonWidget({
    required this.icon,
    this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Icon(icon, size: 26),
      style: TextButton.styleFrom(
        primary: Colors.white,
        backgroundColor: const Color(0xFF4C4F5E),
        shape: const CircleBorder(),
        elevation: 0,
        padding: const EdgeInsets.all(12),
      ),
    );
  }
}
