import 'package:imc_calculator/constants.dart';
import 'package:flutter/material.dart';
import 'package:imc_calculator/widgets/card_widget.dart';
import 'package:imc_calculator/widgets/round_icon_button_widget.dart';

class InformationCardWidget extends StatelessWidget {
  final String title;
  final String value;
  final VoidCallback increment;
  final VoidCallback decrement;
  const InformationCardWidget({
    required this.title,
    required this.value,
    required this.increment,
    required this.decrement,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: CardWideget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(title, style: kLabelTextStyle),
            Text(value, style: kNumberTextSytle),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RoundIconButtonWidget(
                  icon: Icons.remove,
                  onPressed: decrement,
                ),
                // const SizedBox(width: 12),
                RoundIconButtonWidget(
                  icon: Icons.add,
                  onPressed: increment,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
