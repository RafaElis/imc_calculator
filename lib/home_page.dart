import 'package:imc_calculator/calaculate_imc.dart';
import 'package:imc_calculator/constants.dart';
import 'package:imc_calculator/result_page.dart';
import 'package:imc_calculator/widgets/bottom_button_widget.dart';
import 'package:imc_calculator/widgets/card_widget.dart';
import 'package:imc_calculator/widgets/gender_card_widget.dart';
import 'package:imc_calculator/widgets/information_card_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String selectedGender = 'male';
  double heigthValue = 150;
  int weigth = 60;
  int age = 20;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('IMC CALCULATOR'),
        elevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                GenderCardWidget(
                  'male',
                  selectedGender: selectedGender,
                  onPressed: () => setState(() {
                    selectedGender = 'male';
                  }),
                ),
                GenderCardWidget(
                  'female',
                  selectedGender: selectedGender,
                  onPressed: () => setState(() {
                    selectedGender = 'female';
                  }),
                ),
              ],
            ),
          ),
          Expanded(
            child: SizedBox(
              width: double.infinity,
              child: CardWideget(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text('ALTURA', style: kLabelTextStyle),
                    const SizedBox(height: 4),
                    RichText(
                      text: TextSpan(
                        text: "$heigthValue",
                        style: kNumberTextSytle,
                        children: const <TextSpan>[
                          TextSpan(text: 'cm', style: kLabelTextStyle),
                        ],
                      ),
                    ),
                    Slider.adaptive(
                      value: heigthValue,
                      min: 60.0,
                      max: 220.0,
                      activeColor: const Color(0xffeb1555),
                      onChanged: (double value) {
                        setState(() {
                          heigthValue =
                              double.parse((value).toStringAsFixed(2));
                        });
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                InformationCardWidget(
                  title: 'PESO (KG)',
                  value: weigth.toString(),
                  increment: () => setState(() {
                    weigth++;
                  }),
                  decrement: () {
                    if (weigth > 1) {
                      setState(() {
                        weigth--;
                      });
                    }
                  },
                ),
                InformationCardWidget(
                  title: 'IDADE',
                  value: age.toString(),
                  increment: () => setState(() {
                    age++;
                  }),
                  decrement: () {
                    if (age > 1) {
                      setState(() {
                        age--;
                      });
                    }
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          BottomButtomWidget(
            text: 'CALCULAR',
            onPressed: () {
              final imc = CalulateImc(weight: weigth, height: heigthValue);
              imc.calculate();
              final String imcResult = imc.imc.toStringAsFixed(2);
              final String resultTitle = imc.results[imc.imcType]['text'];
              final String resultRange = imc.results[imc.imcType]['range'];
              final String imcInterpretation =
                  imc.results[imc.imcType]['interpretation'];
              final Color resultColor = imc.results[imc.imcType]['color'];

              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return ResultPage(
                  resultTitle: resultTitle,
                  interpretation: imcInterpretation,
                  resultColor: resultColor,
                  imc: imcResult,
                  resultRange: resultRange,
                );
              }));
            },
          ),
        ],
      ),
    );
  }
}
