# imc_calculator

## Getting Started

Uma aplicação que calcula o IMC (Índice de massa corporal) do usuário, esse aplicativo foi feito usando flutter.

O visual da aplicação foi baseado no design feito pelo Ruben Vaalt.
- [Link do design original](https://dribbble.com/shots/4585382-Simple-BMI-Calculator)

- App preview

  ![App preview](/assets/preview.png)